package com.fourcatsdev.seguranca;

import com.fourcatsdev.modelo.Usuario;

public class DetalheUsuario {

	private Usuario usuario;

	public DetalheUsuario(Usuario usuario) {
		this.usuario = usuario;		
	}
	
	public boolean isAtivo() {
		return usuario.isAtivo();
	}
	
	public String getNome() {
		return usuario.getNome();
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
